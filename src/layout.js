
import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import 'antd/dist/antd.css';
import * as d3 from "d3";
import sortBy from "lodash/sortBy";
import LineChartExplorer from "./line-chart-explorer";
import LineChart from "./line-chart";
import ScatterPlot from './scatter-plot';

const { Header, Sider, Content, Footer } = Layout;

class MainLayout extends React.Component {

    constructor (props) {
        super(props);

        this.width = 1200;
        this.height = 200;

        this.colorPalette = d3.scaleOrdinal().domain([1,10])
            .range(d3.schemeSet3);

        // Bindings
        this.parseData = this.parseData.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    state = {
        data: [], selection: [],
        scatterData: [],
        collapsed: false,
        collapsedRight: false
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    toggleRight = () => {
        this.setState({
            collapsedRight: !this.state.collapsedRight,
        });
    };

    render() {
        return (
            <Layout>
                <Sider
                    trigger={null} collapsible collapsed={this.state.collapsed}
                    style={{
                        overflow: 'auto',
                        height: '100vh',
                        position: 'fixed',
                        left: 0,
                    }}
                >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span className="nav-text">nav 1</span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span className="nav-text">nav 2</span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <span className="nav-text">nav 3</span>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Icon type="bar-chart" />
                            <span className="nav-text">nav 4</span>
                        </Menu.Item>
                        <Menu.Item key="5">
                            <Icon type="cloud-o" />
                            <span className="nav-text">nav 5</span>
                        </Menu.Item>
                        <Menu.Item key="6">
                            <Icon type="appstore-o" />
                            <span className="nav-text">nav 6</span>
                        </Menu.Item>
                        <Menu.Item key="7">
                            <Icon type="team" />
                            <span className="nav-text">nav 7</span>
                        </Menu.Item>
                        <Menu.Item key="8">
                            <Icon type="shop" />
                            <span className="nav-text">nav 8</span>
                        </Menu.Item>
                    </Menu>
                </Sider>

                <Layout
                    style={{
                        marginLeft: (this.state.collapsed ? 100: 200),
                        transition: 'margin 100ms',
                        marginRight: (this.state.collapsedRight ? 100: 200)
                    }}
                >
                    <Header style={{ background: '#fff', padding: 0 }} >
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />

                        <Icon
                            className="trigger"
                            type={this.state.collapsedRight ? 'menu-fold': 'menu-unfold'}
                            onClick={this.toggleRight}
                            style={{ float: 'right', margin: '1.3em 0'}}
                        />
                    </Header>

                    <Content style={{ margin: '24px 16px 0', overflow: 'scroll', maxHeight: '80vh' }}>


                        <div className={'graph-container'}>
                            <div className={'graph graph-scatter'}>
                                <ScatterPlot
                                    width={this.width}
                                    height={this.width / 2}
                                    data={this.state.scatterData}
                                    selection={this.state.selection}
                                    color={this.colorPalette(0)}
                                    legend={'Scatter Plot'}
                                />
                            </div>
                        </div>

                        <div className={'graph graph-1'}>
                            <LineChart
                                width={this.width}
                                height={this.height}
                                data={this.state.data}
                                selection={this.state.selection}
                                color={this.colorPalette(0)}
                                legend={'Temperature'}
                            />
                        </div>

                        <hr/>

                        <div className={'graph graph-2'}>
                            <LineChart
                                width={this.width}
                                height={this.height}
                                data={this.state.data}
                                selection={this.state.selection}
                                color={this.colorPalette(1)}
                                legend={'Vibrations'}
                            />
                        </div>

                        <hr/>

                        <div className={'graph graph-3'}>
                            <LineChart
                                width={this.width}
                                height={this.height}
                                data={this.state.data}
                                selection={this.state.selection}
                                color={this.colorPalette(2)}
                                legend={'Rotation Speed'}
                            />
                        </div>

                        <hr/>

                        <div className={'graph graph-4'}>
                            <LineChart
                                width={this.width}
                                height={this.height}
                                data={this.state.data}
                                selection={this.state.selection}
                                color={this.colorPalette(3)}
                                legend={'Pressure'}
                            />
                        </div>
                    </Content>

                    <Layout>
                        <Content>
                            <LineChartExplorer
                                width={this.width}
                                height={100}
                                data_collection={
                                    {
                                        // tag: data_object
                                        0: {data: this.state.data, tag: 0, color: this.colorPalette(0)},
                                        1: {data: this.state.data, tag: 1, color: this.colorPalette(1)},
                                        2: {data: this.state.data, tag: 2, color: this.colorPalette(2)},
                                        3: {data: this.state.data, tag: 3, color: this.colorPalette(3)},
                                    }
                                }
                                onSelection={selection => {this.setState({selection})}}
                            />
                        </Content>

                    </Layout>
                </Layout>


                <Sider
                    trigger={null} collapsible collapsed={this.state.collapsedRight}
                    style={{
                        overflow: 'auto',
                        height: '100vh',
                        position: 'fixed',
                        right: 0,
                    }}
                >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span className="nav-text">nav 1</span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span className="nav-text">nav 2</span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <span className="nav-text">nav 3</span>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Icon type="bar-chart" />
                            <span className="nav-text">nav 4</span>
                        </Menu.Item>
                        <Menu.Item key="5">
                            <Icon type="cloud-o" />
                            <span className="nav-text">nav 5</span>
                        </Menu.Item>
                        <Menu.Item key="6">
                            <Icon type="appstore-o" />
                            <span className="nav-text">nav 6</span>
                        </Menu.Item>
                        <Menu.Item key="7">
                            <Icon type="team" />
                            <span className="nav-text">nav 7</span>
                        </Menu.Item>
                        <Menu.Item key="8">
                            <Icon type="shop" />
                            <span className="nav-text">nav 8</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
            </Layout>
        );
    }

    loadData () {
        // Load chart data into state.
        d3.csv(
            "CIMIS_Station_126.csv", this.parseData
        ).then(data => {
            this.setState(
                (prevState, props) => {
                    return Object.assign(prevState, {data: sortBy((prevState.data || []).concat(data), d => d.date)})
                }
            );
        })
    }

    componentDidMount() {
        // Load chart data into state.
        d3.csv(
            "CIMIS_Station_125.csv", this.parseData
        ).then(data => {
            this.setState(
                (prevState, props) => {
                    return Object.assign(prevState, {data: data})
                }
            );
        });

        // Load data for scatter plot into state.
        d3.csv(
            "scatter_plot_data.csv", (d) => {
                d.x = +d.x;
                d.y = +d.y;

                return d;
            }
        ).then(data => {
            this.setState(
                (prevState, props) => {
                    return Object.assign(prevState, {scatterData: data})
                }
            );
        })
    }

    parseData (data_item) {
        data_item.Date = d3.timeParse("%m/%d/%Y %H:%M")(data_item.Date);
        data_item.Air_Temp = +data_item.Air_Temp;
        data_item.date = data_item.Date;
        data_item.value = data_item.Air_Temp;
        return data_item;
    }
}


export default MainLayout;
