import React from 'react';
import rd3 from 'react-d3-library';
import node from './d3file';

import * as d3 from "d3";


export default class D3Component extends React.Component {

    constructor(props) {
        super(props);
        this.state = {d3: '', selected_range: {start: null, end: null}};
        this.updateChart = this.updateChart.bind(this);
        this.renderGraph = this.renderGraph.bind(this);
    }

    updateChart(){

        const margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 860 - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        const svg = d3.select(this.svgEl)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        const updateSelectedArea = (start, end) => {
            console.log(start, end);
            // this.setState({selected_range: {start: start, end: end}});
            this.renderGraph(start, end)
        };

        // Read the data
        d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv",

            // When reading the csv, I must format variables:
            function(d){
                return { date : d3.timeParse("%Y-%m-%d")(d.date), value : d.value }
            },

        ).then(data => {

            // Add X axis --> it is a date format
            var x = d3.scaleTime()
                .domain(d3.extent(data, function(d) { return d.date; }))
                .range([ 0, width ]);
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            // Add Y axis
            var y = d3.scaleLinear()
                .domain([0, d3.max(data, function(d) { return +d.value; })])
                .range([ height, 0 ]);
            svg.append("g")
                .call(d3.axisLeft(y));

            // Add the line
            svg.append("path")
                .datum(data)
                .attr("fill", "none")
                .attr("stroke", "steelblue")
                .attr("stroke-width", 1.5)
                .attr("d", d3.line()
                    .x(function(d) { return x(d.date) })
                    .y(function(d) { return y(d.value) })
                );

            // Add Brush
            const brush = d3.brushX().extent(
                [
                    [margin.left, margin.top], // Top Left Corner
                    [width + margin.left, height + margin.top] // Bottom right corner
                ]
            ).on("start brush end", brushmoved).on('end', updateZoom);

            const gBrush = d3.select(this.svgEl).call(brush);

            // Brush handles
            const brushResizePath = function(d) {
                const e = +(d.type == "e"),
                    x = e ? 1 : -1,
                    y = height / 2;
                return "M" + (.5 * x) + "," + y + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6) + "V" + (2 * y - 6) + "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y) + "Z" + "M" + (2.5 * x) + "," + (y + 8) + "V" + (2 * y - 8) + "M" + (4.5 * x) + "," + (y + 8) + "V" + (2 * y - 8);
            };

            const handle = gBrush.selectAll(".handle--custom")
                .data([{type: "w"}, {type: "e"}])
                .enter().append("path")
                .attr("class", "handle--custom")
                .attr("stroke", "#000")
                .attr("cursor", "ew-resize")
                .attr("d", brushResizePath);

            gBrush.call(brush.move, [0.3, 0.5].map(x));

            function brushmoved() {
                const s = d3.event.selection;
                if (s == null) {
                    handle.attr("display", "none");
                    // circle.classed("active", false);
                } else {
                    // const sx = s.map(x.invert);
                    // updateSelectedArea(sx[0], sx[1]);

                    // circle.classed("active", function(d) { return sx[0] <= d && d <= sx[1]; });
                    handle.attr("display", null).attr("transform", function(d, i) { return "translate(" + [ s[i], - height / 4] + ")"; });
                }
            }

            function updateZoom() {
                const s = d3.event.selection;
                if (s != null) {
                    const sx = s.map(x.invert);
                    updateSelectedArea(sx[0], sx[1]);
                }
            }

        });

        return <div>{node}</div>;
    }

    renderGraph(start, end){
        const margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 860 - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        // Remove existing graph.
        d3.select(this.graphSvgEl).selectAll("*").remove();


        // append the svg object to the body of the page
        const svg = d3.select(this.graphSvgEl)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // Read the data
        d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv",

            // When reading the csv, I must format variables:
            function(d){
                return { date : d3.timeParse("%Y-%m-%d")(d.date), value : d.value }
            },

        ).then(data => {

            if (start && end) {
                data = data.filter( (item) => Boolean(start <= item.date && item.date <= end));
            }

            // Add X axis --> it is a date format
            var x = d3.scaleTime()
                .domain(d3.extent(data, function(d) { return d.date; }))
                .range([ 0, width ]);
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            // Add Y axis
            var y = d3.scaleLinear()
                .domain([0, d3.max(data, function(d) { return +d.value; })])
                .range([ height, 0 ]);
            svg.append("g")
                .call(d3.axisLeft(y));

            // Add the line
            svg.append("path")
                .datum(data)
                .attr("fill", "none")
                .attr("stroke", "steelblue")
                .attr("stroke-width", 1.5)
                .attr("d", d3.line()
                    .x(function(d) { return x(d.date) })
                    .y(function(d) { return y(d.value) })
                );
        });
    }

    render() {
        // set the dimensions and margins of the graph
        const margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 460 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

        return (
            <div>
                <svg
                    width={width}
                    height={height}
                    ref={el => this.graphSvgEl = el} >
                </svg>

                <br/><hr/><br/>

                <svg
                    width={width}
                    height={height}
                    ref={el => this.svgEl = el} >
                </svg>
                <rd3.Component data={this.state.d3} />
            </div>
        )
    }

    componentDidMount() {
        this.updateChart();
        this.renderGraph();
    }

    componentDidUpdate() {
        this.updateChart();
        this.renderGraph();
    }
};
