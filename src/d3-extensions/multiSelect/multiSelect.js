import * as d3 from "d3";

function defaultExtent() {
    const svg = this.ownerSVGElement || this;
    return [[0, 0], [svg.width.baseVal.value, svg.height.baseVal.value]];
}


export default function multiSelect() {
    const __state = {

        // Keep the original brushes and their meta available on the instance.
        brushes: [],

        callbacks: {
            start: ()=>{},
            end: ()=>{},
            brush: ()=>{},
        }
    };


    function multiSelect (selection) {

        __state.g  = selection
            .append('g')
            .attr('class', 'multi-select');


        // Initialize the __state
        initialize(selection);

        // Initialize a brush for the very first (yet empty) selection.
        newBrush();
        drawBrushes();
    }

    function initialize (selection) {
        if(!__state._extent) {
            __state._extent = defaultExtent.apply(selection.node());
        }
    }

    multiSelect.extent = function (extent) {
        __state._extent = extent;

        return multiSelect;
    };

    multiSelect.on = function (type, callback) {
        __state[type] = callback;

        return multiSelect;
    };

    multiSelect.transform = function (previousScale, newScale) {
        let selection, brush,
            brushes = __state.brushes;

        // Modify scales to make sure the values are not clamped.
        // Otherwise we will lose selections once the go off-screen.
        previousScale = previousScale.copy().clamp(false);
        newScale = newScale.copy().clamp(false);

        for(let index in brushes) {
            brush = __state.g.select(`#brush-${brushes[index].id}`);
            selection = d3.brushSelection(brush.node());

            if(selection) {
                // transform selection to new scale.
                brush.call(
                    brushes[index].brush.move,
                    selection.map(previousScale.invert).map(newScale)
                );
            }
        }
    };


    /* CREATE NEW BRUSH
     *
     * This creates a new brush. A brush is both a function (in our array) and a set of predefined DOM elements
     * Brushes also have selections. While the selection are empty (i.e. a suer hasn't yet dragged)
     * the brushes are invisible. We will add an initial brush when this viz starts. (see end of file)
     * Now imagine the user clicked, moved the mouse, and let go. They just gave a selection to the initial brush.
     * We now want to create a new brush.
     * However, imagine the user had simply dragged an existing brush--in that case we would not want to create a new one.
     * We will use the selection of a brush in brushend() to differentiate these cases.
     */
    function newBrush() {
        const brush = d3.brushX()
            .extent(__state._extent)
            .on("start", brushStart)
            .on("brush", brushed)
            .on("end", brushEnd);

        __state.brushes.push({
            id: __state.brushes.length,
            brush: brush
        });

        function brushStart() {
            // Call registered callbacks.
            __state.callbacks.start(d3.brushSelection(this));
        }

        function brushed() {
            // Call registered callbacks.
            __state.callbacks.brush(d3.brushSelection(this));
        }

        function brushEnd() {

            // Figure out if our latest brush has a selection
            const lastBrushID = __state.brushes[__state.brushes.length - 1].id;
            const lastBrush = __state.g.select('#brush-' + lastBrushID).node();
            const selection = d3.brushSelection(lastBrush);

            // If it does, that means we need another one
            if (selection && selection[0] !== selection[1]) {
                // selectedBrush = brushes.length - 1;
                newBrush();
            }
            else {
                // selectedBrush = this.dataset.id;

            }

            // Always draw brushes
            drawBrushes();

            // Call registered callbacks.
            __state.callbacks.end(d3.brushSelection(this));
        }
    }

    function drawBrushes() {

        const brushSelection = __state.g
            .selectAll('.brush')
            .data(__state.brushes, function (d){return d.id});

        // Set up new brushes
        brushSelection.enter()
            .insert("g", '.brush')
            .attr('class', 'brush')
            .attr('id', function(brush){ return "brush-" + brush.id; })
            .attr('data-id', function(brush){ return brush.id; })
            .call( (brushObject) => brushObject.brush)
            .each(function(brushObject) {
                //call the brush
                brushObject.brush(d3.select(this));
            });

        /* REMOVE POINTER EVENTS ON BRUSH OVERLAYS
         *
         * This part is a bit tricky and requires knowledge of how brushes are implemented.
         * They register pointer events on a .overlay rectangle within them.
         * For existing brushes, make sure we disable their pointer events on their overlay.
         * This frees the overlay for the most current (as of yet with an empty selection) brush to listen for click and drag events
         * The moving and resizing is done with other parts of the brush, so that will still work.
         */
        brushSelection
            .each(function (brushObject){
                d3.select(this)
                    .attr('class', 'brush')
                    .selectAll('.overlay')
                    .style('pointer-events', function() {
                        if (brushObject.id === __state.brushes.length-1 && brushObject.brush !== undefined) {
                            return 'all';
                        } else {
                            return 'none';
                        }
                    })
            });

        brushSelection.exit()
            .remove();
    }

    return multiSelect;
}
