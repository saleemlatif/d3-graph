function defaultExtent() {
    const svg = this.ownerSVGElement || this;
    return [[0, 0], [svg.width.baseVal.value, svg.height.baseVal.value]];
}

export default function tooltip() {
    const __state = {color: "#4682B4"};

    function tooltip(selection) {

        // Initialize the state
        initialize(selection);

        const {color} = __state;
        const [[x1, y1], [, y2]] = __state._extent;

        __state.g = selection
            .append('g')
            .attr('class', 'tooltip')
            .attr('opacity', 0);

        // Render the vertical line.
        __state.g
            .append('line')
            .attr('class', 'tooltip-line')
            .attr('x1', x1)
            .attr('x2', x1) // we want a vertical line.
            .attr('y1', y1)
            .attr('y2', y2)
            .attr('stroke', color);

        // Render the text Box and text
        __state.box = __state.g
            .append('g')
            .attr('transform', `translate(${10}, ${-15})`)
            .attr('class', 'text-box');

        // Render the circle to be shown on the path.
        __state.box
            .append('circle')
            .attr('class', 'tooltip-info')
            .attr('r', 5)
            .attr('fill', 'none')
            .attr('stroke', color);

        // Render the rectangle containing the text
        __state.box
            .append('rect')
            .attr('transform', `translate(${10}, ${-15})`)
            .attr('width', 50)
            .attr('height', 30)
            .attr('fill', color);

        // Render the text
        __state.box
            .append('text')
            .attr('transform', `translate(${10}, ${-15})`)
            .attr('x', 5)
            .attr('y', 20)
            .attr('fill', 'white')
            .attr('text-anchor', 'start')
    }

    function initialize (selection) {
        if(!__state._extent) {
            __state._extent = defaultExtent.apply(selection.node());
        }
    }

    // Method to allow tooltip to be updated dynamically.
    // Mainly a hook for mouse events to update the position and text.
    tooltip.update = function (x, y, text) {

        // Move the vertical line horizontally.
        __state.g
            .select('line')
            .transition()
            .duration(50)
            .attr('transform', `translate(${x})`);

        // Move the text box vertically and horizontally.
        __state.box
            .transition()
            .duration(50)
            .attr('transform', `translate(${x}, ${y})`);

        // Update the text
        __state.box
            .select('text')
            .text(text);

        return tooltip;
    };

    tooltip.hide = function () {
        __state.g
            .attr('opacity', 0)
    };

    tooltip.show = function () {
        __state.g
            .attr('opacity', 1)
    };

    tooltip.extent = function (extent) {
        __state._extent = extent;

        return tooltip;
    };

    tooltip.color = function (color) {
        __state.color = color;

        return tooltip;
    };

    return tooltip;
}
