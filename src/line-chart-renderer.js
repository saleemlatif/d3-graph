import React from 'react';

import * as d3 from 'd3';
import sortBy from 'lodash/sortBy';


import LineChart from './line-chart';
import ScatterPlot from './scatter-plot';
import LineChartExplorer from './line-chart-explorer';


class LineChartRenderer extends React.Component {
    constructor (props) {
        super(props);
        this.state = { data: [], selection: []};

        this.width = 1200;
        this.height = 200;

        this.container = 'container';

        this.colorPalette = d3.scaleOrdinal().domain([1,10])
            .range(d3.schemeSet3);

        // Bindings
        this.parseData = this.parseData.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    render () {
        return (
            <React.Fragment>

                <div className={'graph-container'}>
                    <div className={'graph graph scatter'}>
                        <ScatterPlot
                            width={this.width}
                            height={this.width / 2}
                            data={this.state.data}
                            selection={this.state.selection}
                            color={this.colorPalette(0)}
                            legend={'Scatter Plot'}
                        />
                    </div>
                </div>

                <div className={'graph-container'}>
                    <div className={'graph graph-1'}>
                        <LineChart
                            width={this.width}
                            height={this.height}
                            data={this.state.data}
                            selection={this.state.selection}
                            color={this.colorPalette(0)}
                            legend={'Temperature'}
                        />
                    </div>

                    <hr/>

                    <div className={'graph graph-2'}>
                        <LineChart
                            width={this.width}
                            height={this.height}
                            data={this.state.data}
                            selection={this.state.selection}
                            color={this.colorPalette(1)}
                            legend={'Vibrations'}
                        />
                    </div>

                    <hr/>

                    <div className={'graph graph-3'}>
                        <LineChart
                            width={this.width}
                            height={this.height}
                            data={this.state.data}
                            selection={this.state.selection}
                            color={this.colorPalette(2)}
                            legend={'Rotation Speed'}
                        />
                    </div>

                    <hr/>

                    <div className={'graph graph-4'}>
                        <LineChart
                            width={this.width}
                            height={this.height}
                            data={this.state.data}
                            selection={this.state.selection}
                            color={this.colorPalette(3)}
                            legend={'Pressure'}
                        />
                    </div>

                    <hr/>

                    <div className={'explorer'}>
                        <LineChartExplorer
                            width={this.width}
                            height={100}
                            data_collection={
                                {
                                    // tag: data_object
                                    0: {data: this.state.data, tag: 0, color: this.colorPalette(0)},
                                    1: {data: this.state.data, tag: 1, color: this.colorPalette(1)},
                                    2: {data: this.state.data, tag: 2, color: this.colorPalette(2)},
                                    3: {data: this.state.data, tag: 3, color: this.colorPalette(3)},
                                }
                            }
                            onSelection={selection => {this.setState({selection})}}
                        />
                    </div>
                </div>

                <button onClick={this.loadData}>Load More Data</button>

            </React.Fragment>
        );
    }

    loadData () {
        // Load chart data into state.
        d3.csv(
            "CIMIS_Station_126.csv", this.parseData
        ).then(data => {
            this.setState(
                (prevState, props) => {
                    return Object.assign(prevState, {data: sortBy((prevState.data || []).concat(data), d => d.date)})
                }
            );
        })

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("Previous State: ", prevState, "Next State: ", this.state);
    }

    componentDidMount() {
        // Load chart data into state.
        d3.csv(
            "CIMIS_Station_125.csv", this.parseData
        ).then(data => {
            this.setState(
                (prevState, props) => {
                    return Object.assign(prevState, {data: data})
                }
            );
        })
    }

    parseData (data_item) {
        data_item.Date = d3.timeParse("%m/%d/%Y %H:%M")(data_item.Date);
        data_item.Air_Temp = +data_item.Air_Temp;
        data_item.date = data_item.Date;
        data_item.value = data_item.Air_Temp;
        return data_item;
    }
}


// LineChartRenderer.PropTypes = {};


export default LineChartRenderer;
