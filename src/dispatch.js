class Dispatch {
    constructor () {
        this.listeners = {};
    }

    listen (event_type, callback) {
        if(this.listeners[event_type]) {
            this.listeners[event_type].push(callback);
        }
        else {
            this.listeners[event_type] = [callback];
        }
    }

    notify (event_type, arg) {
        for(let callback of (this.listeners[event_type] || [])) {
            callback(arg);
        }
    }
}
const dispatcher = new Dispatch();

export default dispatcher;
