import React from 'react';

import * as d3 from "d3";


export default class ChartExplorerComponent extends React.Component {

    constructor(props) {
        super(props);

        console.log("ChartExplorerComponent: constructor", props);

        // Bindings
        this.renderChart = this.renderChart.bind(this);
    }

    renderChart(){
        const {data, zoom, brushedFunc} = this.props;

        var svg = d3.select(this.svgEl),
            // margin = {top: 20, right: 20, bottom: 110, left: 40},
            margin2 = {top: 430, right: 20, bottom: 30, left: 40},
            width = +svg.attr("width") - margin2.left - margin2.right,
            // height = +svg.attr("height") - margin2.top - margin2.bottom,
            height2 = +svg.attr("height") - margin2.top - margin2.bottom;

        // Remove existing chart.
        svg.selectAll("*").remove();

        // var x = d3.scaleTime().range([0, width]),
        var x2 = d3.scaleTime().range([0, width]),
        // var y = d3.scaleLinear().range([height, 0]),
            y2 = d3.scaleLinear().range([height2, 0]);

        // var xAxis = d3.axisBottom(x),
        var xAxis2 = d3.axisBottom(x2);
        //     yAxis = d3.axisLeft(y);

        var brush = d3.brushX()
            .extent([[0, 0], [width, height2]])
            .on("brush end", brushed);

        // TODO: Take as a prop.
        // var zoom = d3.zoom()
        //     .scaleExtent([1, Infinity])
        //     .translateExtent([[0, 0], [width, height]])
        //     .extent([[0, 0], [width, height]])
        zoom.on("zoom", zoomed);

        // var line = d3.line()
        //     .x(function (d) { return x(d.Date); })
        //     .y(function (d) { return y(d.Air_Temp); });
        //
        var line2 = d3.line()
            .x(function (d) { return x2(d.Date); })
            .y(function (d) { return y2(d.Air_Temp); });

        // var clip = svg.append("defs").append("svg:clipPath")
        //     .attr("id", "clip")
        //     .append("svg:rect")
        //     .attr("width", width)
        //     .attr("height", height)
        //     .attr("x", 0)
        //     .attr("y", 0);


        // var Line_chart = svg.append("g")
        //     .attr("class", "focus")
        //     .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        //     .attr("clip-path", "url(#clip)");
        //

        // var focus = svg.append("g")
        //     .attr("class", "focus")
        //     .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        //
        var context = svg.append("g")
            .attr("class", "context")
            .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

        // x.domain(d3.extent(data, function(d) { return d.Date; }));
        // y.domain([0, d3.max(data, function (d) { return d.Air_Temp; })]);
        x2.domain(d3.extent(data, function(d) { return d.Date; }));
        y2.domain([0, d3.max(data, function (d) { return d.Air_Temp; })]);


        // focus.append("g")
        //     .attr("class", "axis axis--x")
        //     .attr("transform", "translate(0," + height + ")")
        //     .call(xAxis);
        //
        // focus.append("g")
        //     .attr("class", "axis axis--y")
        //     .call(yAxis);
        //
        // Line_chart.append("path")
        //     .datum(data)
        //     .attr("class", "line")
        //     .attr("d", line);

        context.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line2);


        context.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height2 + ")")
            .call(xAxis2);

        context.append("g")
            .attr("class", "brush")
            .call(brush)
            .call(brush.move, x2.range());

        // svg.append("rect")
        //     .attr("class", "zoom")
        //     .attr("width", width)
        //     .attr("height", height)
        //     .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        //     .call(zoom);


        function brushed() {
            if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
            var s = d3.event.selection || x2.range();


            // Call the Prop func
            brushedFunc(s);
            // x.domain(s.map(x2.invert, x2));
            // Line_chart.select(".line").attr("d", line);
            // focus.select(".axis--x").call(xAxis);
            // svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
            //     .scale(width / (s[1] - s[0]))
            //     .translate(-s[0], 0));
        }

        function zoomed() {
            if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
            var t = d3.event.transform;
            x.domain(t.rescaleX(x2).domain());
            Line_chart.select(".line").attr("d", line);
            focus.select(".axis--x").call(xAxis);
            context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
        }

    }

    render() {
        return (
            <div className={'chart'}>
                <svg
                    width={960}
                    height={500}
                    ref={el => this.svgEl = el} >
                </svg>
            </div>
        )
    }

    componentDidMount() {
        this.renderChart();
    }

    componentDidUpdate() {
        this.renderChart();
    }
};
