import React from 'react';
import PropTypes from 'prop-types';

import * as d3 from 'd3';
import * as d3Ext from './d3-extensions';


class ScatterPlot extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            data: this.props.data,
            selection: this.props.selection
        };

        this.margin = {right: 10, left: 20, top: 10, bottom: 20};
        this.width = props.width;
        this.height = props.height;

        this.legendHeight = 20;

        // derived attributes.
        this.innerWidth = this.width - this.margin.left - this.margin.right;
        this.innerHeight = this.height - this.margin.top - this.margin.bottom - this.legendHeight;

        // Bindings
        this.zoomIn = this.zoomIn.bind(this);
    }

    shouldComponentUpdate (nextProps, nextState) {
        // Component should be rendered only once, frequent updates must be handled manually to boost performance.
        if (ScatterPlot.hasDataChanged(nextProps, nextState)) {
            this.updateChart(nextProps.data);
        }
        // if (ScatterPlot.hasSelectionChanged(nextProps, nextState)) {
        //     this.zoomIn(nextProps.selection);
        //     this.setState({selection: nextProps.selection});
        // }

        return false;
    }

    render () {
        const {data} = this.state;
        const {color, legend} = this.props;

        this.xScale = this.getXScale(data);
        this.yScale = this.getYScale(data);

        this.xAxis = d3.axisBottom(this.xScale);
        this.yAxis = d3.axisLeft(this.yScale);

        this.multiSelect = d3Ext.multiSelect()
            .extent([[0, 0], [this.innerWidth, this.innerHeight]]);

        return (
            <svg
                className={'chart'}
                pointerEvents={'none'}
                viewBox={`0 0 ${this.width} ${this.height}`}
                ref={node => this.svg = node}
            >
                <defs>`
                    <clipPath id={'clip'}>
                        <rect
                            width={this.innerWidth}
                            height={this.innerHeight + this.legendHeight}
                            x={0}
                            y={0}
                        >
                        </rect>
                    </clipPath>
                </defs>

                <g
                    transform={`translate(${this.margin.left}, ${this.margin.top + this.legendHeight})`}
                    ref={node => this.container = node}
                >
                    <g
                        className={'legend'}
                        transform={`translate(${this.margin.left}, ${-this.legendHeight})`}
                    >
                        <rect
                            style={{'fill': color, 'stroke': color, 'fillOpacity': '0.1'}}
                            width={10}
                            height={10}
                        />
                        <text
                            fontSize={10}
                            textAnchor={'start'}
                            x={12}
                            y={9}
                            fill={color}

                        >{legend}</text>
                    </g>
                    <g
                        className={'axis axis--x'}
                        transform={`translate(0, ${this.innerHeight})`}
                        ref={node => d3.select(node).call(this.xAxis)}
                    />
                    <g
                        className={'axis axis--y'}
                        ref={node => d3.select(node).call(this.yAxis)}
                    />

                    <g
                        className={'scatter-plot-container'}
                    />
                    <g
                        clipPath="url(#clip)"
                        className={'brushes'}
                        ref={node => d3.select(node).call(this.multiSelect)}
                    />
                </g>
            </svg>
        )
    }

    zoomIn(selection) {
        if (this.props.data.length !== this.state.data.length) return; // Ignore transitions
        if (isNaN(selection[0].getDate()) || isNaN(selection[1].getDate())) return; // Ignore invalid dates

        // const {scatterData: data} = this.state;
        const svg = d3.select(this.svg);
        const prevScale = this.xScale.copy();

        this.xScale.domain(selection);

        this.multiSelect.transform(prevScale, this.xScale);

        svg.select(".axis--x").call(this.xAxis);
        // svg.select(".line").attr("d", this.line(data));
    }

    getXScale(data) {
        return d3.scaleLinear()
            .domain(d3.extent(data, function(data){ return data.x}))
            .range([0, this.innerWidth])
            .clamp(true);
    }

    getYScale(data) {
        return d3.scaleLinear()
            .domain([0, d3.max(data, function (d) {return d.y})])
            .range([this.innerHeight, 0]);
    }

    updateChart (data) {
        const prevScale = this.xScale.copy();
        const {color} = this.props;

        this.xScale.domain(d3.extent(data, function(data){ return data.x}));
        this.yScale.domain([0, d3.max(data, function (d) {return d.y})]);

        const svg = d3.select(this.svg);

        const dots = svg.select(".scatter-plot-container")
            .selectAll('.dot')
            .data(data);

        // Enter
        dots
            .enter()
            .append("circle")
            .attr("cx", d => this.xScale(d.x))
            .attr("cy", d => this.yScale(d.y))
            .attr("r", 1.5)
            .attr('class', 'dot')
            .style("fill", color);


        svg.select('.axis--x')
            .call(this.xAxis);
        svg.select('.axis--y')
            .call(this.yAxis);

        this.multiSelect.transform(prevScale, this.xScale);
    }

    static hasDataChanged (props, state) {
        // Checks if data has differences between props and state.
        return props.data.length !== state.data.length
    }

    static hasSelectionChanged (props, state) {
        // Checks if selection has differences between props and state.
        if (props.selection.length !== state.selection.length)
            return true;

        if (props.selection.length > 0 && state.selection.length > 0)
            return props.selection[0] !== state.selection[0] || props.selection[1] !== state.selection[1];

        return false;
    }
}


ScatterPlot.defaultProps = {
    color: "#4682B4",
    margin: {right: 10, left: 20, top: 10, bottom: 20},
};


ScatterPlot.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    selection: PropTypes.arrayOf(PropTypes.object).isRequired,
    legend: PropTypes.string.isRequired,

    // Optional Attributes
    color: PropTypes.string,
    margin: PropTypes.shape({
        right: PropTypes.number.isRequired,
        left: PropTypes.number.isRequired,
        top: PropTypes.number.isRequired,
        bottom: PropTypes.number.isRequired,
    }),
};


export default ScatterPlot;
