import React from 'react';
import logo from './logo.svg';
import './App.css';
import './chart.css'

// import D3Component from './rd3-component';
// import ChartComponent from './chart-component';
// import ChartExplorer from './chart-explorer';

import * as d3 from "d3";

import LineChartRenderer from './line-chart-renderer';
import MainLayout from './layout';


class App extends React.Component {

  constructor (props) {
    super(props);

    this.state = { data: []};
    this.parseDate = d3.timeParse("%m/%d/%Y %H:%M");

    // Bindings
    this.parseData = this.parseData.bind(this);
  }

  componentDidMount() {
    d3.csv(
        "CIMIS_Station_125.csv", this.parseData
    ).then(data => {
      this.setState(
          (prevState, props) => {
            return Object.assign(prevState, {data: data})
          }
      );
    })
  }

  parseData (data_item) {
    data_item.Date = this.parseDate(data_item.Date);
    data_item.Air_Temp = +data_item.Air_Temp;
    return data_item;
  }

  render () {
    return (
        <div className="App">
          {/*<header className="App-header">*/}
          {/*  <img src={logo} className="App-logo" alt="logo"/>*/}
          {/*  <p>*/}
          {/*    Edit <code>src/App.js</code> and save to reload.*/}
          {/*  </p>*/}
          {/*  <a*/}
          {/*      className="App-link"*/}
          {/*      href="https://reactjs.org"*/}
          {/*      target="_blank"*/}
          {/*      rel="noopener noreferrer"*/}
          {/*  >*/}
          {/*    Learn React*/}
          {/*  </a>*/}
          {/*</header>*/}
          <div className='d3'>
            {/*<D3Component />*/}
            {/*<ChartComponent data={this.state.data}/>*/}
            {/*<ChartExplorer data={this.state.data}/>*/}
            {/*<LineChartRenderer />*/}
            <MainLayout />
          </div>
        </div>
    );
  }
}

export default App;
