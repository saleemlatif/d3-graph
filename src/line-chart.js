import React from 'react';
import PropTypes from 'prop-types';

import * as d3 from 'd3';
import * as d3Ext from './d3-extensions';


class LineChart extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            data: this.props.data,
            selection: this.props.selection
        };

        this.margin = {right: 10, left: 20, top: 10, bottom: 20};
        this.width = props.width;
        this.height = props.height;

        this.legendHeight = 20;

        // derived attributes.
        this.innerWidth = this.width - this.margin.left - this.margin.right;
        this.innerHeight = this.height - this.margin.top - this.margin.bottom - this.legendHeight;

        // Bindings
        this.zoomIn = this.zoomIn.bind(this);
        this.drawPoints = this.drawPoints.bind(this);
    }

    shouldComponentUpdate (nextProps, nextState) {
        // Component should be rendered only once, frequent updates must be handled manually to boost performance.
        if (LineChart.hasDataChanged(nextProps, nextState)) {
            this.updateChart(nextProps.data);
        }
        if (LineChart.hasSelectionChanged(nextProps, nextState)) {
            this.zoomIn(nextProps.selection);
            this.setState({selection: nextProps.selection});
        }

        return false;
    }

    render () {
        const {data} = this.state;
        const {color, legend} = this.props;

        this.xScale = this.getXScale(data);
        this.yScale = this.getYScale(data);

        this.xAxis = d3.axisBottom(this.xScale);
        this.yAxis = d3.axisLeft(this.yScale);

        // D3 Components
        this.line = d3.area()
            .x(d => this.xScale(d.date))
            .y0(this.innerHeight)
            .y1(d => this.yScale(d.Air_Temp));

        this.tooltip = d3Ext.tooltip()
            .extent([[0, 0], [this.innerWidth, this.innerHeight]])
            .color(color);

        this.multiSelect = d3Ext.multiSelect()
            .extent([[0, 0], [this.innerWidth, this.innerHeight]]);

        return (
            <svg
                className={'chart'}
                pointerEvents={'none'}
                viewBox={`0 0 ${this.width} ${this.height}`}
                ref={node => {
                    this.svg = node;
                    d3.select(node)
                        .on('mousemove', this.getOnMouseMove(this))
                        .on('mouseout', () => this.tooltip.hide())
                }}
            >
                <defs>`
                    <clipPath id={'clip'}>
                        <rect
                            width={this.innerWidth}
                            height={this.innerHeight + this.legendHeight}
                            x={0}
                            y={0}
                        >
                        </rect>
                    </clipPath>
                </defs>

                <g
                    transform={`translate(${this.margin.left}, ${this.margin.top + this.legendHeight})`}
                    ref={node => this.container = node}
                >
                    <g
                        className={'legend'}
                        transform={`translate(${this.margin.left}, ${-this.legendHeight})`}
                    >
                        <rect
                            style={{'fill': color, 'stroke': color, 'fillOpacity': '0.1'}}
                            width={10}
                            height={10}
                        />
                        <text
                            fontSize={10}
                            textAnchor={'start'}
                            x={12}
                            y={9}
                            fill={color}

                        >{legend}</text>
                    </g>
                    <g
                        className={'axis axis--x'}
                        transform={`translate(0, ${this.innerHeight})`}
                        ref={node => d3.select(node).call(this.xAxis)}
                    />
                    <g
                        className={'axis axis--y'}
                        ref={node => d3.select(node).call(this.yAxis)}
                    />
                    <path
                        className={'line'}
                        d={this.line(data)}
                        style={{'fill': color, 'stroke': color, 'fillOpacity': '0.1'}}
                        clipPath="url(#clip)"
                    />
                    <g
                        className={'circle-container'}
                        ref={node => this.circle_container = node}
                        clipPath="url(#clip)"
                    />
                    <g
                        id={'tooltip-g'}
                        ref={node => {d3.select(node).call(this.tooltip)}}
                        // opacity={0}
                    >
                    </g>
                    <g
                        clipPath="url(#clip)"
                        className={'brushes'}
                        ref={node => d3.select(node).call(this.multiSelect)}
                    />
                </g>
            </svg>
        )
    }

    zoomIn(selection) {
        if (this.props.data.length !== this.state.data.length) return; // Ignore transitions
        if (isNaN(selection[0].getDate()) || isNaN(selection[1].getDate())) return; // Ignore invalid dates

        const {data} = this.state;
        const svg = d3.select(this.svg);
        const prevScale = this.xScale.copy();

        this.xScale.domain(selection);

        this.multiSelect.transform(prevScale, this.xScale);

        svg.select(".axis--x").call(this.xAxis);
        svg.select(".line").attr("d", this.line(data));

        this.drawPoints();
    }

    getXScale(data) {
        return d3.scaleTime()
            .domain(d3.extent(data, function(data){ return data.date}))
            .range([0, this.innerWidth])
            .clamp(true);
    }

    getYScale(data) {
        return d3.scaleLinear()
            .domain([0, d3.max(data, function (d) {return d.value})])
            .range([this.innerHeight, 0]);
    }

    updateChart (data) {
        const prevScale = this.xScale.copy();

        this.xScale.domain(d3.extent(data, function(data){ return data.date}));
        this.yScale.domain([0, d3.max(data, function (d) {return d.value})]);

        const svg = d3.select(this.svg);

        svg.select(".line")
            .transition()
            .duration(1)
            .ease(d3.easeLinear)
            .attr("d", this.line(data))
            .on("end", () => {
                this.setState({data});
            });

        svg.select('.axis--x')
            .call(this.xAxis);
        svg.select('.axis--y')
            .call(this.yAxis);

        this.drawPoints();
        this.multiSelect.transform(prevScale, this.xScale);
    }

    drawPoints () {
        // Currently, we are using uniform distribution of x axis.
        const
            ticks = this.xScale.ticks().map(this.xScale);

        const points  = d3.select(this.circle_container).selectAll(".circle")
            .data(
                ticks || []
            );
        const line = d3.select(this.svg).select('.line').node();

        if (!this.getPosition(line, 0).y) {
            // wait for the line to be drawn.
            setTimeout(this.drawPoints, 50);
            return;
        }

        // Add points on entry
        points
            .enter()
            .append('circle')
            .attr('class', 'circle')
            .attr('r', 3.5)
            .attr('fill', this.props.color);

        // Update all points
        points
            .attr('cx', x => x)
            .attr('cy', x => this.getPosition(line, x).y);

        // Remove all points on exit.
        points.exit().remove()
    }

    getOnMouseMove(component){
        // do not bind to react element.
        return function () {
            // Show the tooltip if hidden.
            component.tooltip.show();

            const mousePosition = d3.mouse(this);
            component.updateTooltip(
                ...[
                    mousePosition[0] - component.margin.left,
                    mousePosition[1]
                ]
            )
        };
    }

    updateTooltip (x) {
        const line = d3.select(this.svg).select('.line').node(),
            position = this.getPosition(line, x);

        this.tooltip.update(position.x, position.y, this.yScale.invert(position.y).toFixed(2))
    }

    // Find the (x, y) point on the `line` having x-coordinate `x`
    getPosition(line, x) {
        let beginning = 0,
            // Since we are using d3.area, innerWidth needs to be subtracted to ignore the last
            // line used to complete the path.
            end = line.getTotalLength() - this.innerWidth,
            target,
            pos;

        if (d3.select(line).attr('d') === null) {
            return {};
        }

        while (true){
            target = Math.floor((beginning + end) / 2);

            pos = line.getPointAtLength(target);
            if ((target === end || target === beginning) && pos.x !== x) {
                break;
            }
            if (pos.x > x) end = target; // Continue search in the left half.
            else if (pos.x < x) beginning = target;  // Continue search in the right half.
            else break; //position found
        }

        return pos
    }

    static hasDataChanged (props, state) {
        // Checks if data has differences between props and state.
        return props.data.length !== state.data.length
    }

    static hasSelectionChanged (props, state) {
        // Checks if selection has differences between props and state.
        if (props.selection.length !== state.selection.length)
            return true;

        if (props.selection.length > 0 && state.selection.length > 0)
            return props.selection[0] !== state.selection[0] || props.selection[1] !== state.selection[1];

        return false;
    }
}


LineChart.defaultProps = {
    color: "#4682B4",
    margin: {right: 10, left: 20, top: 10, bottom: 20},
};


LineChart.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    selection: PropTypes.arrayOf(PropTypes.object).isRequired,
    legend: PropTypes.string.isRequired,

    // Optional Attributes
    color: PropTypes.string,
    margin: PropTypes.shape({
        right: PropTypes.number.isRequired,
        left: PropTypes.number.isRequired,
        top: PropTypes.number.isRequired,
        bottom: PropTypes.number.isRequired,
    }),
};


export default LineChart;
