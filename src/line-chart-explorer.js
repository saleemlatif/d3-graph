import React from 'react';
import PropTypes from 'prop-types';
import every from 'lodash/every';
import isEqual from 'lodash/isEqual';

import * as d3 from 'd3';


class LineChartExplorer extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            data_collection: this.props.data_collection, selection: []
        };

        this.container = 'explorer-container';
        this.margin = {right: 10, left: 20, top: 10, bottom: 20};
        this.width = props.width;
        this.height = props.height;

        // Derived attributes.
        this.innerWidth = this.width - this.margin.left - this.margin.right;
        this.innerHeight = this.height - this.margin.top - this.margin.bottom;
        this.stackingHeight = this.innerHeight * 0.25;
        this.brush = null;

        // Bindings
        this.brushed = this.brushed.bind(this);
        this.updateChart = this.updateChart.bind(this);
        this.drawPaths = this.drawPaths.bind(this);
    }

    shouldComponentUpdate (nextProps, nextState) {
        // Component should be rendered only once, frequent updates must be handled manually to boost performance.
        if (LineChartExplorer.hasDataChanged(nextProps, nextState))
        {
            const previousSelection = this.state.selection.map(this.xScale.invert);

            this.updateChart(
                Object.keys(nextProps.data_collection).map(key => nextProps.data_collection[key])
            );

            // Avoid infinite loop, delay the state update.
            setTimeout(
                () => this.updateState(
                    nextProps.data_collection,
                    previousSelection.map(this.xScale)),
                50
            );
        }
        return false;
    }

    render() {
        const {data_collection} = this.state;
        const {data} = Object.values(data_collection)[0]; // Use the first one to get started

        this.xScale = this.getXScale(data, [0, this.innerWidth]);
        this.yScale = this.getYScale(data, [this.innerHeight, 0]);

        this.xAxis = d3.axisBottom(this.xScale);

        // D3 Components
        this.line = d3.line()
            .x(d => this.xScale(d.Date))
            .y(d => this.yScale(d.Air_Temp));

        this.brush = d3.brushX()
            .extent([[0, -this.stackingHeight], [this.innerWidth, this.innerHeight]])
            .on("brush", this.brushed);


        return (
            <svg
                className={'chart'}
                viewBox={`0 0 ${this.width} ${this.height + this.stackingHeight}`}
                ref={node => this.svg = node}
            >
                <g
                    className={this.container}
                    transform={`translate(${this.margin.left}, ${this.margin.top + this.innerHeight * 0.25})`}
                >
                    <g
                        className={'axis axis--x'}
                        transform={`translate(0, ${this.innerHeight})`}
                        ref={node => d3.select(node).call(this.xAxis)}
                    />
                    <g
                        className={'.charts'}
                        ref={node => this.chart_container = node}
                    />
                    <g
                        className={'brush'}
                        ref={node => d3.select(node).call(this.brush).call(this.brush.move, this.xScale.range())}
                    />

                </g>
            </svg>
        );
    }

    getLine(stackingRatio) {
        return d3.line()
            .x(d => this.xScale(d.Date))
            .y(d => this.yScale(d.Air_Temp) - this.stackingHeight * stackingRatio);
    }

    updateState (data_collection, selection) {
        const svg = d3.select(this.svg);

        this.setState(
            {
                data_collection: data_collection,
                selection: selection
            },
            () => {
                svg.select('.brush').call(
                    this.brush.move,
                    this.state.selection[0] !== undefined ? this.state.selection : this.xScale.range()
                )
            }
        )

    }

    brushed() {
        const chart = d3.select(this.svg);

        const selection = d3.event.selection || this.xScale.range();
        const selection_domain = selection.map(this.xScale.invert, this.xScale);

        this.props.onSelection(selection_domain);
        this.setState({selection});

        chart.select(".axis--x").call(this.xAxis);
    }

    getXScale(data, range) {
        return d3.scaleTime()
            .domain(d3.extent(data, function(data){ return data.Date}))
            .range(range);
    }

    getYScale(data, range) {
        return d3.scaleLinear()
            .domain([0, d3.max(data, function (d) {return d.Air_Temp})])
            .range(range);
    }

    updateChart (data_list) {
        const [{data}] = data_list;

        this.xScale.domain(d3.extent(data, function(data){ return data.Date}));
        this.yScale.domain([0, d3.max(data, function (d) {return d.Air_Temp})]);


        const svg = d3.select(this.svg);

        this.drawPaths(data_list);

        svg.select('.axis--x')
            .call(this.xAxis);
    }

    drawPaths (data_list) {
        const lines  = d3.select(this.chart_container).selectAll(".line")
            .data(data_list);
        const line_count = data_list.length;

        // Add points on entry
        lines
            .enter()
            .append('g')
            .append('path')
            .attr('class', 'line')
            .style('stroke', d => d.color)
            .attr('d', (d, i) =>  this.getLine(1 - i/line_count)(d.data));

        // Update all points
        lines
            .style('stroke', d => d.color)
            .attr('d', (d, i) => this.getLine(1 - i/line_count)(d.data));

        // Remove all points on exit.
        lines.exit().remove()

    }

    static hasDataChanged (props, state) {
        // If some data sets are added ot removed, data collection is changed.
        if(!isEqual(Object.keys(props.data_collection), Object.keys(state.data_collection))) return true;

        if(!every(Object.keys(props.data_collection).map(
            key => props.data_collection[key].data.length === state.data_collection[key].data.length)
        )) return true;

        return false;
    }
}

LineChartExplorer.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    data_collection: PropTypes.object.isRequired,
    onSelection: PropTypes.func,
};

export default LineChartExplorer;
